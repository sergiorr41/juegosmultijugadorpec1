
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public GameObject[] mainMenuTabs;
    public Image[] tankIcons;
    public Text roundCounter;
    public Text[] playerControlSelector;
    public int maxRounds = 20;
    public AudioClip clickSound;
    public GameObject exitButton;

    private AudioSource source;
    private int activeTab = 0;
    private int numPlayers = 2;
    private int numRounds = 5;
    public int minRounds = 1;
    private int minPlayers = 2;
    private int maxPlayers = 4;
    private List<KeyValuePair<string, string>> possibleInputs;

    private int[] selectedControls = { 0, 1, 2, 3 };

    public enum UIMode { 
        PLAYERS,
        ROUNDS
    }

    void Start()
    {
        source = GetComponent<AudioSource>();

        possibleInputs = new List<KeyValuePair<string, string>>();
        possibleInputs.Add(new KeyValuePair<string, string>("Keyboard1", "Teclado WASD"));
        possibleInputs.Add(new KeyValuePair<string, string>("Keyboard2", "Teclado Flechas"));
        possibleInputs.Add(new KeyValuePair<string, string>("Keyboard3", "Teclado IJKL"));
        possibleInputs.Add(new KeyValuePair<string, string>("Keyboard4", "Teclado Num�rico"));
        possibleInputs.Add(new KeyValuePair<string, string>("GamePad", "Mando"));
        RenderActiveTab(); 
    }

    // Update is called once per frame
    void Update()
    {
        //PlayerSelection
        if (activeTab == 1) 
        {
            RenderPlayerNumber();
            roundCounter.text = numRounds + "";
        }
        //PlayerSelection
        if (activeTab == 2)
        {
            RenderPlayerController();
        }
        exitButton.SetActive(activeTab > 0);
    }

    private void RenderPlayerNumber()
    {
        for (int idx = 0; idx < tankIcons.Length; idx++)
            tankIcons[idx].enabled = idx < numPlayers;
    }

    private void RenderPlayerController()
    {
        for (int idx = 0; idx < playerControlSelector.Length; idx++)
        {
            playerControlSelector[idx].gameObject.SetActive(idx < numPlayers);
            if (idx < numPlayers)
                playerControlSelector[idx].text = possibleInputs[selectedControls[idx]].Value;
        }
    }

    private void RenderActiveTab()
    {
        for (int idx = 0; idx < mainMenuTabs.Length; idx++)
            mainMenuTabs[idx].SetActive(idx == activeTab);
        if (activeTab == 0)
            InputSystem.onAnyButtonPress.CallOnce(ctrl => NextTab());
    }

    public void Play()
    {
        source.PlayOneShot(clickSound, 1f);
        PlayerPrefs.SetInt("PlayerNumber", numPlayers);
        PlayerPrefs.SetInt("RoundsToWin", numRounds);
        for(int idx = 0; idx < numPlayers; idx ++) 
        {
            PlayerPrefs.SetString("P" + (idx +1) + "Scheme", possibleInputs[selectedControls[idx]].Key);
        }
        SceneManager.LoadScene(1);
    }

    public void NextTab()
    {
        source.PlayOneShot(clickSound, 1f);
        if (activeTab < mainMenuTabs.Length - 1)
            activeTab++;
        else
            activeTab = mainMenuTabs.Length -1;
        RenderActiveTab();
    }

    public void PreviousTab()
    {
        source.PlayOneShot(clickSound, 1f);
        if (activeTab > 0)
            activeTab--;
        else
            activeTab = 0;
        RenderActiveTab();
    }

    public void Substract(string mode)
    {
        source.PlayOneShot(clickSound, 1f);
        int minValue = minPlayers;
        int currentValue = numPlayers;
        if (UIMode.ROUNDS.ToString().Equals(mode)) {
            minValue = minRounds;
            currentValue = numRounds;
        }

        if (currentValue > minValue)
            currentValue--;
        else
            currentValue = minValue;

        if (UIMode.ROUNDS.ToString().Equals(mode))
            numRounds = currentValue;
        else if (UIMode.PLAYERS.ToString().Equals(mode))
        {
            numPlayers = currentValue;
            selectedControls = new int[] { 0, 1, 2, 3 };
        }
    }

    public void Add(string mode)
    {
        source.PlayOneShot(clickSound, 1f);
        int maxValue = maxPlayers;
        int currentValue = numPlayers;
        if (UIMode.ROUNDS.ToString().Equals(mode))
        {
            maxValue = maxRounds;
            currentValue = numRounds;
        }

        if (currentValue < maxValue)
            currentValue++;
        else
            currentValue = maxValue;

        if (UIMode.ROUNDS.ToString().Equals(mode))
            numRounds = currentValue;
        else if (UIMode.PLAYERS.ToString().Equals(mode))
        {
            numPlayers = currentValue;
            selectedControls = new int[]{ 0, 1, 2, 3 };
        }
        source.PlayOneShot(clickSound, 1f);
    }

    public void NextControl(int playerNumber)
    {
        source.PlayOneShot(clickSound, 1f);
        int currentControl = selectedControls[playerNumber - 1];
        bool foundNext = false;
        while (!foundNext) {
            if (currentControl < possibleInputs.Count -1)
                currentControl++;
            else
                currentControl = 0;
            foundNext = !possibleInputs[currentControl].Key.StartsWith("Keyboard") || !ControlAlreadySelected(currentControl);
        }
        selectedControls[playerNumber - 1] = currentControl;
    }

    public void PreviousControl(int playerNumber)
    {
        source.PlayOneShot(clickSound, 1f);
        int currentControl = selectedControls[playerNumber - 1];
        bool foundNext = false;
        while (!foundNext)
        {
            if (currentControl > 0)
                currentControl--;
            else
                currentControl = possibleInputs.Count -1;
            foundNext = !possibleInputs[currentControl].Key.StartsWith("Keyboard") || !ControlAlreadySelected(currentControl);
        }
        selectedControls[playerNumber - 1] = currentControl;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private bool ControlAlreadySelected(int controlToCheck)
    {
        for (int idx = 0; idx < numPlayers; idx++)
        {
            if (selectedControls[idx] == controlToCheck)
                return true;
        }
        return false;
    }
}
