using Cinemachine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Complete
{
    public class GameManager : MonoBehaviour
    {
        public int m_NumRoundsToWin = 5;            // The number of rounds a single player has to win to win the game
        public float m_StartDelay = 3f;             // The delay between the start of RoundStarting and RoundPlaying phases
        public float m_EndDelay = 3f;               // The delay between the end of RoundPlaying and RoundEnding phases
        public CameraControl m_CameraControl;       // Reference to the CameraControl script for control during different phases
        public Text m_MessageText;                  // Reference to the overlay Text to display winning text, etc.
        public Image m_Background;                  // Reference to the overlay Text to display winning text, etc.
        public GameObject m_TankPrefab;             // Reference to the prefab the players will control
        public TankManager[] m_Tanks;               // A collection of managers for enabling and disabling different aspects of the tanks
        public GameObject m_MinimapCamera;          
        public GameObject m_TankCamera;


        private int m_RoundNumber;                  // Which round the game is currently on
        private WaitForSeconds m_StartWait;         // Used to have a delay whilst the round starts
        private WaitForSeconds m_EndWait;           // Used to have a delay whilst the round or game ends
        private TankManager m_RoundWinner;          // Reference to the winner of the current round.  Used to make an announcement of who won
        private TankManager m_GameWinner;           // Reference to the winner of the game.  Used to make an announcement of who won
        private Rect[] twiceDivided = { new Rect(0.0f, 0.5f, 1f, 0.5f), new Rect(0.0f, 0.0f, 1f, 0.5f) };
        private Rect[] fourTimesDivided = { new Rect(0.0f, 0.5f, 0.5f, 0.5f), new Rect(0.5f, 0.5f, 0.5f, 0.5f), new Rect(0.0f, 0.0f, 0.5f, 0.5f), new Rect(0.5f, 0.0f, 0.5f, 0.5f) };
        private string[] playersControlSchemes;
        private int joinedPlayers;
        private int playerNumber;
        private List<string> basicLayerNames;
        private GameObject minimapCam;
        private PlayerInputManager playerInputManager;
        private bool playerJoined = false;

        private void Start()
        {
            // Create the delays so they only have to be made once
            m_StartWait = new WaitForSeconds (m_StartDelay);
            m_EndWait = new WaitForSeconds (m_EndDelay);
            playerNumber = PlayerPrefs.GetInt("PlayerNumber", 2);
            m_NumRoundsToWin = PlayerPrefs.GetInt("RoundsToWin", 5);
            playersControlSchemes = new string[4]{ PlayerPrefs.GetString("P1Scheme", "Keyboard1"), 
                                                    PlayerPrefs.GetString("P2Scheme", "Keyboard2"), 
                                                    PlayerPrefs.GetString("P3Scheme", "GamePad"), 
                                                    PlayerPrefs.GetString("P4Scheme", "GamePad")};
            joinedPlayers = playerNumber;
            playerInputManager = GetComponent<PlayerInputManager>();
            playerInputManager.DisableJoining();

            InitLayers();

            SpawnAllTanks();
            SetCameraTargets();

            // Once the tanks have been created and the camera is using them as targets, start the game
            StartCoroutine (GameLoop());
        }

        private void InitLayers()
        {
            basicLayerNames = new List<string>();
            Regex rx = new Regex("P. Cam", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            for (int i = 0; i <= 31; i++) //user defined layers start with layer 8 and unity supports 31 layers
            {
                var layerN = LayerMask.LayerToName(i); //get the name of the layer
                if (layerN.Length > 0 && !rx.IsMatch(layerN)) //only add the layer if it has been named (comment this line out if you want every layer)
                    basicLayerNames.Add(layerN);
            }
        }


        private void SpawnAllTanks()
        {
            Camera mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();
            for (int i = 0; i < m_Tanks.Length; i++)
			{
                if (i < joinedPlayers)
                {
                    InputDevice device = playersControlSchemes[i].StartsWith("Keyboard") ? Keyboard.current : null;
                    var input = PlayerInput.Instantiate(m_TankPrefab, controlScheme: playersControlSchemes[i], pairWithDevice: device);
                    m_Tanks[i].m_Instance = input.gameObject;
                    m_Tanks[i].m_PlayerNumber = i + 1;
                    m_Tanks[i].StoreInput();
                    m_Tanks[i].Reset();
                    m_Tanks[i].Setup();
                    AddCamera(i);
                }
                else if (i >= joinedPlayers && playerNumber > 2) 
                    AddMinimap(i);
            }
            mainCam.gameObject.SetActive(false);
        }

		private void AddCamera (int i)
        {
            string layerName = "P" + (i+1) + " Cam";
            int layerNumber = LayerMask.NameToLayer(layerName);
            List<string> layers = new List<string>();
            layers.AddRange(basicLayerNames);
            layers.Add(layerName);

            GameObject childCam = Instantiate(m_TankCamera);
            SetActiveAllChildren(childCam.transform);
            childCam.layer = layerNumber;
 			Camera newCam = childCam.GetComponentInChildren<Camera>();
            newCam.enabled = true;
            newCam.gameObject.layer = layerNumber;
            newCam.cullingMask = LayerMask.GetMask(layers.ToArray());
            CinemachineVirtualCamera vcam = childCam.GetComponentInChildren<CinemachineVirtualCamera>();
            vcam.gameObject.layer = layerNumber;
            vcam.LookAt = m_Tanks[i].m_Instance.transform;
            vcam.Follow = m_Tanks[i].m_Instance.transform;

            childCam.transform.parent = m_Tanks[i].m_Instance.transform;

            Rect[] rectArrayToUse = playerNumber > 2 ? fourTimesDivided : twiceDivided;
            newCam.rect = rectArrayToUse[i];
        }

        private void AdjustCameras() {
            int cameraIndex = 0;
            Destroy(minimapCam);
            minimapCam = null;
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                if (i < joinedPlayers && !m_Tanks[i].IsDead())
                {
                    Rect[] rectArrayToUse = playerNumber > 2 ? fourTimesDivided : twiceDivided;
                    SetActiveAllChildren(m_Tanks[i].m_Instance.transform);
                    Camera newCam = m_Tanks[i].m_Instance.GetComponentInChildren<Camera>();
                    newCam.enabled = true;
                    newCam.rect = rectArrayToUse[cameraIndex];
                    cameraIndex++;
                }
                else if (playerNumber > 2)
                {
                    AddMinimap(cameraIndex);
                    cameraIndex++;
                }
            }
            SetCameraTargets();
        }

        private void AddMinimap(int i)
        {
            minimapCam = Instantiate(m_MinimapCamera) as GameObject;
            Camera newCam = minimapCam.GetComponent<Camera>();
            newCam.rect = fourTimesDivided[i];
        }

        private void SetCameraTargets()
        {
            // Create a collection of transforms the same size as the number of tanks
            Transform[] targets = new Transform[playerNumber];

            // For each of these transforms...
            int targetIndex = 0;
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... set it to the appropriate tank transform
                if (i < joinedPlayers && !m_Tanks[i].IsDead())
                {
                    targets[targetIndex] = m_Tanks[i].m_Instance.transform;
                    targetIndex++;
                }
            }

            // These are the targets the camera should follow
            m_CameraControl.m_Targets = targets;
            m_CameraControl.SetStartPositionAndSize();
        }


        // This is called from start and will run each phase of the game one after another
        private IEnumerator GameLoop()
        {
            // Start off by running the 'RoundStarting' coroutine but don't return until it's finished
            yield return StartCoroutine (RoundStarting());

            // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished
            yield return StartCoroutine (RoundPlaying());

            // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished
            yield return StartCoroutine (RoundEnding());

            // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found
            if (m_GameWinner != null)
            {
                // If there is a game winner, restart the level
                SceneManager.LoadScene (0);
            }
            else
            {
                // If there isn't a winner yet, restart this coroutine so the loop continues
                // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end
                StartCoroutine (GameLoop());
            }
        }


        private IEnumerator RoundStarting()
        {
            playerInputManager.DisableJoining();
            // As soon as the round starts reset the tanks and make sure they can't move
            ResetAllTanks();
            DisableTankControl();

            // Snap the camera's zoom and position to something appropriate for the reset tanks
            m_CameraControl.SetStartPositionAndSize();

            // Increment the round number and display text showing the players what round it is
            m_RoundNumber++;
            m_MessageText.text = "ROUND " + m_RoundNumber;
            m_Background.enabled = true;

            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_StartWait;
        }


        private IEnumerator RoundPlaying()
        {
            playerInputManager.EnableJoining();
            // As soon as the round begins playing let the players control the tanks
            EnableTankControl();

            // Clear the text from the screen
            m_MessageText.text = string.Empty;
            m_Background.enabled = false;

            // While there is not one tank left...
            while (GetTankNumber() > 1)
            {
                // ... return on the next frame
                yield return null;
            }
        }


        private IEnumerator RoundEnding()
        {
            // Stop tanks from moving
            DisableTankControl();

            // Clear the winner from the previous round
            m_RoundWinner = null;

            // See if there is a winner now the round is over
            m_RoundWinner = GetRoundWinner();

            // If there is a winner, increment their score
            if (m_RoundWinner != null)
                m_RoundWinner.m_Wins++;

            // Now the winner's score has been incremented, see if someone has one the game
            m_GameWinner = GetGameWinner();

            // Get a message based on the scores and whether or not there is a game winner and display it
            string message = EndMessage();
            m_MessageText.text = message;
            m_Background.enabled = true;

            // Wait for the specified length of time until yielding control back to the game loop
            yield return m_EndWait;
        }


        // This is used to check if there is one or fewer tanks remaining and thus the round should end
        private int GetTankNumber()
        {
            if (playerJoined)
            {
                AssignMissingTanks();
            }
            // Start the count of tanks left at zero
            var tankNumber = 0;

            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if they are active, increment the counter
                if (i < joinedPlayers && !m_Tanks[i].IsDead())
                    tankNumber++;
            }
            if (tankNumber != playerNumber)
            {
                playerNumber = tankNumber;
                AdjustCameras();
            }

            return playerNumber;
        }

        // This function is to find out if there is a winner of the round
        // This function is called with the assumption that 1 or fewer tanks are currently active
        private TankManager GetRoundWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if one of them is active, it is the winner so return it
                if (!m_Tanks[i].IsDead())
                {
                    return m_Tanks[i];
                }
            }

            // If none of the tanks are active it is a draw so return null
            return null;
        }


        // This function is to find out if there is a winner of the game
        private TankManager GetGameWinner()
        {
            // Go through all the tanks...
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                // ... and if one of them has enough rounds to win the game, return it
                if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                {
                    return m_Tanks[i];
                }
            }

            // If no tanks have enough rounds to win, return null
            return null;
        }


        // Returns a string message to display at the end of each round.
        private string EndMessage()
        {
            // By default when a round ends there are no winners so the default end message is a draw
            string message = "DRAW!";

            // If there is a winner then change the message to reflect that
            if (m_RoundWinner != null)
            {
                message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";
            }

            // Add some line breaks after the initial message
            message += "\n\n\n\n";

            // Go through all the tanks and add each of their scores to the message
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                if(i < joinedPlayers)
                    message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
            }

            // If there is a game winner, change the entire message to reflect that
            if (m_GameWinner != null)
            {
                message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";
            }

            return message;
        }


        // This function is used to turn all the tanks back on and reset their positions and properties
        private void ResetAllTanks()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                if(i < joinedPlayers)
                    m_Tanks[i].Reset();
            }
        }


        private void EnableTankControl()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                if (i < joinedPlayers)
                    m_Tanks[i].EnableControl();
            }
        }


        private void DisableTankControl()
        {
            for (int i = 0; i < m_Tanks.Length; i++)
            {
                if (i < joinedPlayers)
                    m_Tanks[i].DisableControl();
            }
        }

        public void OnPlayerJoined(PlayerInput playerInput)
        {
            if (!Array.Exists(m_Tanks, tank => tank.m_Instance.Equals(playerInput.gameObject))) {
                playerJoined = true;
            }
        }

        private void AssignMissingTanks() { 
            GameObject[] tanks = GameObject.FindGameObjectsWithTag("Tank");
            foreach (GameObject tank in tanks) {
                if (!Array.Exists(m_Tanks, item => item.m_Instance.Equals(tank))) {
                    m_Tanks[joinedPlayers].m_Instance = tank;
                    m_Tanks[joinedPlayers].m_PlayerNumber = joinedPlayers + 1;
                    m_Tanks[joinedPlayers].StoreInput();
                    m_Tanks[joinedPlayers].Reset();
                    m_Tanks[joinedPlayers].Setup();
                    joinedPlayers++;
                    playerNumber++;
                    AddCamera(joinedPlayers - 1);
                    AdjustCameras();
                }
            }
            playerJoined = false;
        }

        private void SetActiveAllChildren(Transform transform)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(true);
                SetActiveAllChildren(child);
            }
        }
    }
}